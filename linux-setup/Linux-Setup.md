SETUP
=====

Image installieren und einrichten
---------------------------------

- USB-Boot: mit ventoy - Ventoy2Disk GUI: iso-Datei: manjaro KDE Plasma (full)


WLAN-Treiber
------------

* WLAN-Treiber: wiki.archlinux.org/title/Network_configuration/Wireless
* `lspci -k` --> Wireless:

```
3:00.0 Network controller: Realtek Semiconductor Co., Ltd. RTL8822CE 802.11ac PCIe Wireless Network Adapter
        DeviceName: WLAN
        Subsystem: Hewlett-Packard Company RTL8822CE 802.11ac PCIe Wireless Network Adapter
        Kernel driver in use: rtw_8822ce
        Kernel modules: rtw88_8822ce
```

* `ip link`
* `ip link set wl01 up` #operation not permitted
  * -> `rfkill` # -> Wlan soft-blocked
  * -> `rfkill unblock wlan`

______________

**Treiber für ... nicht unterstützt:**

* Treiber heruntergeladen von https://de.drvhub.net/devices/network/realtek/rtl8822ce-802-11ac-pcie-adapter
* https://wiki.archlinux.org/title/Network_configuration/Wireless#ndiswrapper
* `pamac install wondershaper-git`
* `sudo wondershaper -a wlo1 -u 512 -d 2048` # 600 Kibps
* Andere Konfigs:
  * `# sudo wondershaper -c -a wlo1`
  * `# sudo wondershaper -a wlo1 -u 512 -d 2048`
* Name des Adapters wlo1 mithilfe von `ip addr` gefunden
* Datei /etc/systemd/wondershaper.conf:
  * https://github.com/magnific0/wondershaper/#persistent-usage-of-wondershaper-optional
  * https://raw.githubusercontent.com/magnific0/wondershaper/master/wondershaper.conf

```conf
   IFACE="wlo1"
   DSPEED="2048"
   USPEED="512"
```
+ sudo systemctl enable --now wondershaper.service

_____________________

pacman findet "extra"-AUR Repo nicht - Fix: andere Mirrors auswählen und AUR upgraden...
# aktuelle mirrors konfigurieren
sudo pacman-mirrors -f 0
pacman -Syu

____________

**ndiswrapper installieren - selbst kompilieren**

* https://sourceforge.net/projects/ndiswrapper/files/latest/download (version 1.63)
* https://www.youtube.com/watch?v=v0Ist9aEKEg (Anleitung)
* Datei verschieben nach `~/custom_sys/src/ndiswrapper-<version>`
* und dort entpacken nach `~/custom_sys/src/ndiswrapper-<version>/ndiswrapper-<version>`
* cd ~/custom_sys/src/ndiswrapper-1.63
* tar -xzf ndiswrapper-1.63.tar.gz
* cd ~/custom_sys/src/ndiswrapper-1.63/ndiswrapper-1.63
* make
  - hat fehler. Im Endeffekt fehlen Kernel-Header:
  - sudo pacman -S linux-headers
  - dazu mit uname -r die richtige header-version zum aktuellen Kernel finden und dann wählen
* pamac install kio-admin # Dateioperationen als admin ausführen anstatt "sudo dolphin" auszuführen
* pamak install make

**WLAN-Treiber für Realtek**

über https://linux-hardware.org/?id=pci:10ec-c822-1a3b-3750
--> auf https://github.com/lwfinger/rtw88

```bash
cd ~/custom_sys/src/rtw88
git clone https://aur.archlinux.org/rtw88-dkms-git.git
cd rtw88-dkms-git
makepkg -sri

cd ~/custom_sys/src/rtw88
git clone https://github.com/lwfinger/rtw88.git
cd rtw88
make
sudo make install
```

**Blacklisting vorhandener Treiber: in "/etc/modprobe.d/blacklist.conf"**

```
# Alte Kernel-Module für den WLAN-Treiber RTL8822CE auskommentieren,
# damit ein besserer/neuerer Treiber konfliktfrei greifen kann, gefunden mit lsmod | grep 'rtw88_'
blacklist rtw88_8822ce
blacklist rtw88_8822c
blacklist rtw88_pci
blacklist rtw88_core
# auch diese?
blacklist mac80211
blacklist cfg80211
```

- Neustart
- dann mit `lsmod | grep rtw` prüfen, dass nur module rtw_ (nicht rtw88_) da sind. Dann passt es.

Linux-GUI-Einstellungen
=======================

**KDE Konsole anpassen, um blinkenden Cursor zu erzeugen**

- neues Profil anlegen 
- dort im Tab Cursor die Einstellung vornehmen

**Tastaturkürzel anpassen**

- Systemeinstellungen -> Shortcuts/Kurzbefehle
- -> KWin -> dort Einstellungen vornehmen
        - Meta + hoch / Meta + Herunter -> Fenster Maximieren

Programme installieren
======================

* flatpak > freetube
* `pamac install brave-browser`
* `pamac remove firefox`
* `pamac install thunderbird`
* `pamac install joplin-desktop`
*pCloud
  * `pamac install pcloud-drive`
  * alternativ https://www.pcloud.com/de/download-free-online-cloud-file-storage.html
* `pamac install freefilesync-bin`
* `pamac install freecad`
* `pamac remove openscad`
* `pamac install telegram-desktop`
* `pamac install manjaro-pipewire` -> optionen 2-5
* `pamac install ripgrep`
* `pamac install fd`
* Rust:
  * `pamac install rustup`
  * optionale Abhängigkeiten: lldb: rust-lldb script + gdb: rust-gdb script
  * `rustup default stable`

**fzf**

* `pamac install fzf`
* folgendes in ~/.zshrc einfügen:
  
```
        source /usr/share/fzf/key-bindings.zsh
        source /usr/share/fzf/completion.zsh
        export FZF_DEFAULT_COMMAND='fd . --hidden --exclude ".git,.cache"'
        export FZF_DEFAULT_OPTS='--preview "if [[ -d {} ]]; then ls -la {}; else head -n 60 {}; fi" --preview-label="Vorschau"'
```

**joplin**

```bash
        sudo npm -g install joplin  
        joplin config sync.target 7
        joplin sync
        joplin config edit vim
        joplin config notes.sortOrder.field title
        joplin config notes.sortOrder.reverse false
        joplin config layout.folderList.factor 1
        joplin config layout.noteList.factor 2
        joplin config layout.note.factor 4
```
