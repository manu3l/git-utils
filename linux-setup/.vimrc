"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" VIM-Konfiguration
"
"    -> General
"    -> VIM user interface
"    -> Colors and Fonts
"    -> Files and backups
"    -> Text, tab and indent related
"    -> Visual mode related
"    -> Moving around, tabs and buffers
"    -> Status line
"    -> Editing mappings
"    -> vimgrep searching and cope displaying
"    -> Spell checking
"    -> Misc
"    -> Helper functions
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Disable compatibility with vi which can cause unexpected issues.
set nocompatible

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sets how many lines of history VIM has to remember
set history=1000

" File type detection
filetype on " Enable type file detection. Vim will be able to try to detect the type of file is use.
filetype plugin on " EEnable plugins and load plugin for the detected file type.
filetype indent on " Load an indent file for the detected file type.

" Set to auto read when a file is changed from the outside
set autoread "update file, if not changed inside VIM
au FocusGained,BufEnter,CursorHold * silent! checktime "Warn, if file changed

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
" TODO passt das?
let mapleader = ","

" Fast saving
" TODO passt das?
"" nmap <leader>w :w!<cr>

" :W sudo saves the file
" (useful for handling the permission-denied error)
""command! W execute 'w !sudo tee % > /dev/null' <bar> edit!


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Add numbers to the file.
set number

" Highlight cursor line underneath the cursor horizontally.
set cursorline

" Highlight cursor line underneath the cursor vertically.
set cursorcolumn

" Do not let the cursor scroll below or above N number of lines when scrolling.
set scrolloff=10

" Enable auto completion menu after pressing TAB.
set wildmenu

" Make wildmenu behave like similar to Bash completion.
set wildmode=list:longest

" There are certain files that we would never want to edit with Vim.
" Wildmenu will ignore files with these extensions.
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx
if has("win16") || has("win32")
    set wildignore+=.git\*,.hg\*,.svn\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
endif

" Always show current position
set ruler

" Height of the command bar
set cmdheight=1

" A buffer becomes hidden when it is abandoned
set hidden

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Search options
set ignorecase " Ignore character case when searching
set hlsearch " Highlight search results
set incsearch " While searching though a file incrementally highlight matching characters as you type.
set smartcase " Override the ignorecase option if searching for capital letters.

" Show partial command you type in the last line of the screen.
set showcmd

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch

" How many tenths of a second to blink when matching brackets
set matchtime=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set timeoutlen=500

" Add a bit extra margin to the left
set foldcolumn=1

" Show the mode you are on the last line.
set showmode

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
syntax enable

" Set regular expression engine automatically
set regexpengine=0

" Enable 256 colors palette in Gnome Terminal
if $COLORTERM == 'gnome-terminal'
    set t_Co=256
endif

try
    colorscheme desert
catch
endtry

set background=dark

" Set extra options when running in GUI mode
" TODO passt das?
if has("gui_running")
    set guioptions-=T
    set guioptions-=e
    set t_Co=256
    set guitablabel=%M\ %t
endif

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set fileformats=unix,dos,mac


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Turn backup off, since most stuff is in SVN, git etc. anyway...
set nobackup
set nowb
set noswapfile


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
set expandtab

" On start of line when pressing tab respect existing whitespace
set smarttab

" 1 tab == 2 spaces
set shiftwidth=2
set tabstop=2

" Behaviour for soft-wrap linebreaks 
set linebreak "don't break lines inside a word
set breakindent "preserve indentation when line starts with whitespace
set showbreak=\ >\ "show symbol indication soft-wrapped line continuation

set wrap 
set autoindent " indent newly opened line like the previous
set smartindent " sensible indents for e.g. {, }, #, 'if, for, ...


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs, windows and buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Map <Space> to / (search) and Ctrl-<Space> to ? (backwards search)
" TODO: Leader-Konzept
map <space> /
map <C-space> ?

" Disable highlight when <leader><cr> is pressed
" TODO
map <silent> <leader><cr> :noh<cr>

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

map <leader>l :bnext<cr>
map <leader>h :bprevious<cr>

" Useful mappings for managing tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove
map <leader>t<leader> :tabnext<cr>

" Let 'tl' toggle between this and the last accessed tab
let g:lasttab = 1
nmap <leader>tl :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()


" Opens a new tab with the current buffer's path
" Super useful when editing files in the same directory
map <leader>te :tabedit <C-r>=escape(expand("%:p:h"), " ")<cr>/

" Switch CWD to the directory of the open buffer
map <leader>cd :cd %:p:h<cr>:pwd<cr>

" Specify the behavior when switching between buffers
try
  set switchbuf=useopen,usetab,newtab
  set stal=2
catch
endtry

" Return to last edit position when opening files (You want this!)
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif


""""""""""""""""""""""""""""""
" => Status line
""""""""""""""""""""""""""""""
" Always show the status line
set laststatus=2

" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Zeile:\ %l\ \ Spalte:\ %c

"TODO
" Clear status line when vimrc is reloaded.
""" set statusline=

" Status line left side.
""" set statusline+=\ %F\ %M\ %Y\ %R

" Use a divider to separate the left side from the right side.
""" set statusline+=%=

" Status line right side.
""" set statusline+=\ ascii:\ %b\ hex:\ 0x%B\ row:\ %l\ col:\ %c\ percent:\ %p%%

" Show the status on the second to last line.
""" set laststatus=2

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remap VIM 0 to first non-blank character
map 0 ^

" Move a line of text using ALT+[jk] or Command+[jk] on mac
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

if has("mac") || has("macunix")
  nmap <D-j> <M-j>
  nmap <D-k> <M-k>
  vmap <D-j> <M-j>
  vmap <D-k> <M-k>
endif

" Delete trailing white space on save, useful for some filetypes ;)
fun! CleanExtraSpaces()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfun

if has("autocmd")
    autocmd BufWritePre *.txt,*.js,*.py,*.wiki,*.sh,*.coffee :call CleanExtraSpaces()
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Spell checking
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>

" Shortcuts using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Misc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Toggle paste mode on and off
map <leader>pp :setlocal paste!<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    endif
    return ''
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" 
""" call plug#begin('~/.vim/plugged')
""" 
""" Plug 'preservim/nerdtree'
""" """ 
""" call plug#end()
""" 


"""""""""""""" Weiteres """"""""""""""""
"
"" MAPPINGS --------------------------------------------------------------- {{{
"
"" Set the backslash as the leader key.
"let mapleader = "\"
"
"" Press \\ to jump back to the last cursor position.
"nnoremap <leader>\ ``
"
"" Press \p to print the current file to the default printer from a Linux operating system.
"" View available printers:   lpstat -v
"" Set default printer:       lpoptions -d <printer_name>
"" <silent> means do not display output.
"nnoremap <silent> <leader>p :%w !lp<CR>
"
"" Type jj to exit insert mode quickly.
"inoremap jj <Esc>
"
"" Press the space bar to type the : character in command mode.
"nnoremap <space> :
"
"" Pressing the letter o will open a new line below the current one.
"" Exit insert mode after creating a new line above or below the current line.
"nnoremap o o<esc>
"nnoremap O O<esc>
"
"" Center the cursor vertically when moving to the next word during a search.
"nnoremap n nzz
"nnoremap N Nzz
"
"" Yank from cursor to the end of line.
"nnoremap Y y$
"
"" Map the F5 key to run a Python script inside Vim.
"" We map F5 to a chain of commands here.
"" :w saves the file.
"" <CR> (carriage return) is like pressing the enter key.
"" !clear runs the external clear screen command.
"" !python3 % executes the current file with Python.
"nnoremap <f5> :w <CR>:!clear <CR>:!python3 % <CR>
"
"" You can split the window in Vim by typing :split or :vsplit.
"" Navigate the split view easier by pressing CTRL+j, CTRL+k, CTRL+h, or CTRL+l.
"nnoremap <c-j> <c-w>j
"nnoremap <c-k> <c-w>k
"nnoremap <c-h> <c-w>h
"nnoremap <c-l> <c-w>l
"
"" Resize split windows using arrow keys by pressing:
"" CTRL+UP, CTRL+DOWN, CTRL+LEFT, or CTRL+RIGHT.
"noremap <c-up> <c-w>+
"noremap <c-down> <c-w>-
"noremap <c-left> <c-w>>
"noremap <c-right> <c-w><
"
"" NERDTree specific mappings.
"" Map the F3 key to toggle NERDTree open and close.
"nnoremap <F3> :NERDTreeToggle<cr>
"
"" Have nerdtree ignore certain files and directories.
"let NERDTreeIgnore=['\.git$', '\.jpg$', '\.mp4$', '\.ogg$', '\.iso$', '\.pdf$', '\.pyc$', '\.odt$', '\.png$', '\.gif$', '\.db$']
"
"" }}}
"
"" VIMSCRIPT -------------------------------------------------------------- {{{
"
"" Enable the marker method of folding.
"augroup filetype_vim
    "autocmd!
    "autocmd FileType vim setlocal foldmethod=marker
"augroup END
"
"" If the current file type is HTML, set indentation to 2 spaces.
"autocmd Filetype html setlocal tabstop=2 shiftwidth=2 expandtab
"
"" If Vim version is equal to or greater than 7.3 enable undofile.
"" This allows you to undo changes to a file even after saving it.
"if version >= 703
    "set undodir=~/.vim/backup
    "set undofile
    "set undoreload=10000
"" MAPPINGS --------------------------------------------------------------- {{{
"
"" Set the backslash as the leader key.
"let mapleader = "\"
"
"" Press \\ to jump back to the last cursor position.
"nnoremap <leader>\ ``
"
"" Press \p to print the current file to the default printer from a Linux operating system.
"" View available printers:   lpstat -v
"" Set default printer:       lpoptions -d <printer_name>
"" <silent> means do not display output.
"nnoremap <silent> <leader>p :%w !lp<CR>
"
"" Type jj to exit insert mode quickly.
"inoremap jj <Esc>
"
"" Press the space bar to type the : character in command mode.
"nnoremap <space> :
"
"" Pressing the letter o will open a new line below the current one.
"" Exit insert mode after creating a new line above or below the current line.
"nnoremap o o<esc>
"nnoremap O O<esc>
"
"" Center the cursor vertically when moving to the next word during a search.
"nnoremap n nzz
"nnoremap N Nzz
"
"" Yank from cursor to the end of line.
"nnoremap Y y$
"
"" Map the F5 key to run a Python script inside Vim.
"" We map F5 to a chain of commands here.
"" :w saves the file.
"" <CR> (carriage return) is like pressing the enter key.
"" !clear runs the external clear screen command.
"" !python3 % executes the current file with Python.
"nnoremap <f5> :w <CR>:!clear <CR>:!python3 % <CR>
"
"" You can split the window in Vim by typing :split or :vsplit.
"" Navigate the split view easier by pressing CTRL+j, CTRL+k, CTRL+h, or CTRL+l.
"nnoremap <c-j> <c-w>j
"nnoremap <c-k> <c-w>k
"nnoremap <c-h> <c-w>h
"nnoremap <c-l> <c-w>l
"
"" Resize split windows using arrow keys by pressing:
"" CTRL+UP, CTRL+DOWN, CTRL+LEFT, or CTRL+RIGHT.
"noremap <c-up> <c-w>+
"noremap <c-down> <c-w>-
"noremap <c-left> <c-w>>
"noremap <c-right> <c-w><
"
"" NERDTree specific mappings.
"" Map the F3 key to toggle NERDTree open and close.
"nnoremap <F3> :NERDTreeToggle<cr>
"
"" Have nerdtree ignore certain files and directories.
"let NERDTreeIgnore=['\.git$', '\.jpg$', '\.mp4$', '\.ogg$', '\.iso$', '\.pdf$', '\.pyc$', '\.odt$', '\.png$', '\.gif$', '\.db$']
"
"" }}}
"
"" VIMSCRIPT -------------------------------------------------------------- {{{
"
"" Enable the marker method of folding.
"augroup filetype_vim
    "autocmd!
    "autocmd FileType vim setlocal foldmethod=marker
"augroup END
"
"" If the current file type is HTML, set indentation to 2 spaces.
"autocmd Filetype html setlocal tabstop=2 shiftwidth=2 expandtab
"
"" If Vim version is equal to or greater than 7.3 enable undofile.
"" This allows you to undo changes to a file even after saving it.
"if version >= 703
    "set undodir=~/.vim/backup
    "set undofile
    "set undoreload=10000
"endif
"
"" You can split a window into sections by typing `:split` or `:vsplit`.
"" Display cursorline and cursorcolumn ONLY in active window.
"augroup cursor_off
    "autocmd!
    "autocmd WinLeave * set nocursorline nocursorcolumn
    "autocmd WinEnter * set cursorline cursorcolumn
"augroup END
"
"" If GUI version of Vim is running set these options.
"if has('gui_running')
"
    "" Set the background tone.
    "set background=dark
"
    "" Set the color scheme.
    "colorscheme molokai
"
    "" Set a custom font you have installed on your computer.
    "" Syntax: <font_name>\ <weight>\ <size>
    "set guifont=Monospace\ Regular\ 12
"
    "" Display more of the file by default.
    "" Hide the toolbar.
    "set guioptions-=T
"
    "" Hide the the left-side scroll bar.
    "set guioptions-=L
"
    "" Hide the the left-side scroll bar.
    "set guioptions-=r
"
    "" Hide the the menu bar.
    "set guioptions-=m
"
    "" Hide the the bottom scroll bar.
    "set guioptions-=b
"
    "" Map the F4 key to toggle the menu, toolbar, and scroll bar.
    "" <Bar> is the pipe character.
    "" <CR> is the enter key.
    "nnoremap <F4> :if &guioptions=~#'mTr'<Bar>
        "\set guioptions-=mTr<Bar>
        "\else<Bar>
        "\set guioptions+=mTr<Bar>
        "\endif<CR>
"
"endif
"
"" }}}
"endif
"
"" You can split a window into sections by typing `:split` or `:vsplit`.
"" Display cursorline and cursorcolumn ONLY in active window.
"augroup cursor_off
    "autocmd!
    "autocmd WinLeave * set nocursorline nocursorcolumn
    "autocmd WinEnter * set cursorline cursorcolumn
"augroup END
"
"" If GUI version of Vim is running set these options.
"if has('gui_running')
"
    "" Set the background tone.
    "set background=dark
"
    "" Set the color scheme.
    "colorscheme molokai
"
    "" Set a custom font you have installed on your computer.
    "" Syntax: <font_name>\ <weight>\ <size>
    "set guifont=Monospace\ Regular\ 12
"
    "" Display more of the file by default.
    "" Hide the toolbar.
    "set guioptions-=T
"
    "" Hide the the left-side scroll bar.
    "set guioptions-=L
"
    "" Hide the the left-side scroll bar.
    "set guioptions-=r
"
    "" Hide the the menu bar.
    "set guioptions-=m
"
    "" Hide the the bottom scroll bar.
    "set guioptions-=b
"
    "" Map the F4 key to toggle the menu, toolbar, and scroll bar.
    "" <Bar> is the pipe character.
    "" <CR> is the enter key.
    "nnoremap <F4> :if &guioptions=~#'mTr'<Bar>
        "\set guioptions-=mTr<Bar>
        "\else<Bar>
        "\set guioptions+=mTr<Bar>
        "\endif<CR>
"
"endif
"
"" }}}
