source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
export FZF_DEFAULT_COMMAND='fd . --hidden --exclude ".git,.cache"'
export FZF_DEFAULT_OPTS='--preview "if [[ -d {} ]]; then ls -la {}; else head -n 60 {}; fi" --preview-label="Vorschau"'
