Capslock entfernen
====

1. Datei `disable_caps.reg` herunterladen und als Windows-Admin ausführen. (Funktioniert leider nur für alle Nutzer!!)
2. PC neu starten

Capslock wieder aktivieren
====

1. WIN-Tase drücken, __regedit__ eingeben
2. Pfad `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Keyboard Layout`öffnen
3. Eintrag __Scancode Map__ löschen.
4. PC neu starten.
